package uk.co.unitycoders.pircbotx.data.db;

public class Factoid {
	public String name;
	public String body;

	@Override
	public String toString() {
		return name;
	}
}
