package uk.co.unitycoders.pircbotx.commandprocessor;

import uk.co.unitycoders.pircbotx.modules.AnnotationModule;

public class MockAnnotationModuleNoHelpText extends AnnotationModule {

	public MockAnnotationModuleNoHelpText() {
		super("mock");
	}

}
